# Microsoft Developer Studio Generated NMAKE File, Based on prjButton.dsp
!IF "$(CFG)" == ""
CFG=prjButton - Win32 Debug
!MESSAGE No configuration specified. Defaulting to prjButton - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "prjButton - Win32 Debug" && "$(CFG)" != "prjButton - Win32 Unicode Debug" && "$(CFG)" != "prjButton - Win32 Release MinSize" && "$(CFG)" != "prjButton - Win32 Release MinDependency" && "$(CFG)" != "prjButton - Win32 Unicode Release MinSize" && "$(CFG)" != "prjButton - Win32 Unicode Release MinDependency"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "prjButton.mak" CFG="prjButton - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "prjButton - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "prjButton - Win32 Unicode Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "prjButton - Win32 Release MinSize" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "prjButton - Win32 Release MinDependency" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "prjButton - Win32 Unicode Release MinSize" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "prjButton - Win32 Unicode Release MinDependency" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "prjButton - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\prjBtnGo.dll" ".\prjButton.tlb" ".\prjButton.h" ".\prjButton_i.c" "$(OUTDIR)\prjButton.bsc" ".\Debug\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Button.sbr"
	-@erase "$(INTDIR)\Edit.obj"
	-@erase "$(INTDIR)\Edit.sbr"
	-@erase "$(INTDIR)\Mytoolbar.obj"
	-@erase "$(INTDIR)\Mytoolbar.sbr"
	-@erase "$(INTDIR)\prjButton.obj"
	-@erase "$(INTDIR)\prjButton.pch"
	-@erase "$(INTDIR)\prjButton.res"
	-@erase "$(INTDIR)\prjButton.sbr"
	-@erase "$(INTDIR)\ReflectionWnd.obj"
	-@erase "$(INTDIR)\ReflectionWnd.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\XBand.obj"
	-@erase "$(INTDIR)\XBand.sbr"
	-@erase "$(OUTDIR)\prjBtnGo.dll"
	-@erase "$(OUTDIR)\prjBtnGo.exp"
	-@erase "$(OUTDIR)\prjBtnGo.ilk"
	-@erase "$(OUTDIR)\prjBtnGo.lib"
	-@erase "$(OUTDIR)\prjBtnGo.pdb"
	-@erase "$(OUTDIR)\prjButton.bsc"
	-@erase ".\prjButton.h"
	-@erase ".\prjButton.tlb"
	-@erase ".\prjButton_i.c"
	-@erase ".\Debug\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\prjButton.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=
RSC=rc.exe
RSC_PROJ=/l 0x41f /fo"$(INTDIR)\prjButton.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\prjButton.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Button.sbr" \
	"$(INTDIR)\Edit.sbr" \
	"$(INTDIR)\Mytoolbar.sbr" \
	"$(INTDIR)\prjButton.sbr" \
	"$(INTDIR)\ReflectionWnd.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\XBand.sbr"

"$(OUTDIR)\prjButton.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:yes /pdb:"$(OUTDIR)\prjBtnGo.pdb" /debug /machine:I386 /def:".\prjButton.def" /out:"$(OUTDIR)\prjBtnGo.dll" /implib:"$(OUTDIR)\prjBtnGo.lib" /pdbtype:sept 
DEF_FILE= \
	".\prjButton.def"
LINK32_OBJS= \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\Edit.obj" \
	"$(INTDIR)\Mytoolbar.obj" \
	"$(INTDIR)\prjButton.obj" \
	"$(INTDIR)\ReflectionWnd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\XBand.obj" \
	"$(INTDIR)\prjButton.res"

"$(OUTDIR)\prjBtnGo.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\Debug
TargetPath=.\Debug\prjBtnGo.dll
InputPath=.\Debug\prjBtnGo.dll
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
<< 
	

!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"

OUTDIR=.\DebugU
INTDIR=.\DebugU
# Begin Custom Macros
OutDir=.\DebugU
# End Custom Macros

ALL : "$(OUTDIR)\prjBtnGo.dll" ".\prjButton.tlb" ".\prjButton.h" ".\prjButton_i.c" ".\DebugU\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Edit.obj"
	-@erase "$(INTDIR)\Mytoolbar.obj"
	-@erase "$(INTDIR)\prjButton.obj"
	-@erase "$(INTDIR)\prjButton.pch"
	-@erase "$(INTDIR)\prjButton.res"
	-@erase "$(INTDIR)\ReflectionWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\XBand.obj"
	-@erase "$(OUTDIR)\prjBtnGo.dll"
	-@erase "$(OUTDIR)\prjBtnGo.exp"
	-@erase "$(OUTDIR)\prjBtnGo.ilk"
	-@erase "$(OUTDIR)\prjBtnGo.lib"
	-@erase "$(OUTDIR)\prjBtnGo.pdb"
	-@erase ".\prjButton.h"
	-@erase ".\prjButton.tlb"
	-@erase ".\prjButton_i.c"
	-@erase ".\DebugU\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_UNICODE" /Fp"$(INTDIR)\prjButton.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=
RSC=rc.exe
RSC_PROJ=/l 0x41f /fo"$(INTDIR)\prjButton.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\prjButton.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:yes /pdb:"$(OUTDIR)\prjBtnGo.pdb" /debug /machine:I386 /def:".\prjButton.def" /out:"$(OUTDIR)\prjBtnGo.dll" /implib:"$(OUTDIR)\prjBtnGo.lib" /pdbtype:sept 
DEF_FILE= \
	".\prjButton.def"
LINK32_OBJS= \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\Edit.obj" \
	"$(INTDIR)\Mytoolbar.obj" \
	"$(INTDIR)\prjButton.obj" \
	"$(INTDIR)\ReflectionWnd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\XBand.obj" \
	"$(INTDIR)\prjButton.res"

"$(OUTDIR)\prjBtnGo.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\DebugU
TargetPath=.\DebugU\prjBtnGo.dll
InputPath=.\DebugU\prjBtnGo.dll
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	if "%OS%"=="" goto NOTNT 
	if not "%OS%"=="Windows_NT" goto NOTNT 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	goto end 
	:NOTNT 
	echo Warning : Cannot register Unicode DLL on Windows 95 
	:end 
<< 
	

!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"

OUTDIR=.\ReleaseMinSize
INTDIR=.\ReleaseMinSize
# Begin Custom Macros
OutDir=.\ReleaseMinSize
# End Custom Macros

ALL : "$(OUTDIR)\prjBtnGo.dll" ".\prjButton.tlb" ".\prjButton.h" ".\prjButton_i.c" ".\ReleaseMinSize\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Edit.obj"
	-@erase "$(INTDIR)\Mytoolbar.obj"
	-@erase "$(INTDIR)\prjButton.obj"
	-@erase "$(INTDIR)\prjButton.pch"
	-@erase "$(INTDIR)\prjButton.res"
	-@erase "$(INTDIR)\ReflectionWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\XBand.obj"
	-@erase "$(OUTDIR)\prjBtnGo.dll"
	-@erase "$(OUTDIR)\prjBtnGo.exp"
	-@erase "$(OUTDIR)\prjBtnGo.lib"
	-@erase ".\prjButton.h"
	-@erase ".\prjButton.tlb"
	-@erase ".\prjButton_i.c"
	-@erase ".\ReleaseMinSize\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_DLL" /Fp"$(INTDIR)\prjButton.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=
RSC=rc.exe
RSC_PROJ=/l 0x41f /fo"$(INTDIR)\prjButton.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\prjButton.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\prjBtnGo.pdb" /machine:I386 /def:".\prjButton.def" /out:"$(OUTDIR)\prjBtnGo.dll" /implib:"$(OUTDIR)\prjBtnGo.lib" 
DEF_FILE= \
	".\prjButton.def"
LINK32_OBJS= \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\Edit.obj" \
	"$(INTDIR)\Mytoolbar.obj" \
	"$(INTDIR)\prjButton.obj" \
	"$(INTDIR)\ReflectionWnd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\XBand.obj" \
	"$(INTDIR)\prjButton.res"

"$(OUTDIR)\prjBtnGo.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\ReleaseMinSize
TargetPath=.\ReleaseMinSize\prjBtnGo.dll
InputPath=.\ReleaseMinSize\prjBtnGo.dll
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
<< 
	

!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"

OUTDIR=.\ReleaseMinDependency
INTDIR=.\ReleaseMinDependency
# Begin Custom Macros
OutDir=.\ReleaseMinDependency
# End Custom Macros

ALL : "$(OUTDIR)\prjBtnGo.dll" "$(OUTDIR)\prjButton.bsc" ".\ReleaseMinDependency\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Button.sbr"
	-@erase "$(INTDIR)\Edit.obj"
	-@erase "$(INTDIR)\Edit.sbr"
	-@erase "$(INTDIR)\Mytoolbar.obj"
	-@erase "$(INTDIR)\Mytoolbar.sbr"
	-@erase "$(INTDIR)\prjButton.obj"
	-@erase "$(INTDIR)\prjButton.pch"
	-@erase "$(INTDIR)\prjButton.res"
	-@erase "$(INTDIR)\prjButton.sbr"
	-@erase "$(INTDIR)\ReflectionWnd.obj"
	-@erase "$(INTDIR)\ReflectionWnd.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\XBand.obj"
	-@erase "$(INTDIR)\XBand.sbr"
	-@erase "$(OUTDIR)\prjBtnGo.dll"
	-@erase "$(OUTDIR)\prjBtnGo.exp"
	-@erase "$(OUTDIR)\prjBtnGo.lib"
	-@erase "$(OUTDIR)\prjButton.bsc"
	-@erase ".\prjButton.h"
	-@erase ".\prjButton.tlb"
	-@erase ".\prjButton_i.c"
	-@erase ".\ReleaseMinDependency\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_STATIC_REGISTRY" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\prjButton.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=
RSC=rc.exe
RSC_PROJ=/l 0x41f /fo"$(INTDIR)\prjButton.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\prjButton.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Button.sbr" \
	"$(INTDIR)\Edit.sbr" \
	"$(INTDIR)\Mytoolbar.sbr" \
	"$(INTDIR)\prjButton.sbr" \
	"$(INTDIR)\ReflectionWnd.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\XBand.sbr"

"$(OUTDIR)\prjButton.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\prjBtnGo.pdb" /machine:I386 /def:".\prjButton.def" /out:"$(OUTDIR)\prjBtnGo.dll" /implib:"$(OUTDIR)\prjBtnGo.lib" 
DEF_FILE= \
	".\prjButton.def"
LINK32_OBJS= \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\Edit.obj" \
	"$(INTDIR)\Mytoolbar.obj" \
	"$(INTDIR)\prjButton.obj" \
	"$(INTDIR)\ReflectionWnd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\XBand.obj" \
	"$(INTDIR)\prjButton.res"

"$(OUTDIR)\prjBtnGo.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\ReleaseMinDependency
TargetPath=.\ReleaseMinDependency\prjBtnGo.dll
InputPath=.\ReleaseMinDependency\prjBtnGo.dll
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
<< 
	

!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"

OUTDIR=.\ReleaseUMinSize
INTDIR=.\ReleaseUMinSize
# Begin Custom Macros
OutDir=.\ReleaseUMinSize
# End Custom Macros

ALL : "$(OUTDIR)\prjBtnGo.dll" ".\prjButton.tlb" ".\prjButton.h" ".\prjButton_i.c" ".\ReleaseUMinSize\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Edit.obj"
	-@erase "$(INTDIR)\Mytoolbar.obj"
	-@erase "$(INTDIR)\prjButton.obj"
	-@erase "$(INTDIR)\prjButton.pch"
	-@erase "$(INTDIR)\prjButton.res"
	-@erase "$(INTDIR)\ReflectionWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\XBand.obj"
	-@erase "$(OUTDIR)\prjBtnGo.dll"
	-@erase "$(OUTDIR)\prjBtnGo.exp"
	-@erase "$(OUTDIR)\prjBtnGo.lib"
	-@erase ".\prjButton.h"
	-@erase ".\prjButton.tlb"
	-@erase ".\prjButton_i.c"
	-@erase ".\ReleaseUMinSize\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_UNICODE" /D "_ATL_DLL" /D "_ATL_MIN_CRT" /Fp"$(INTDIR)\prjButton.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=
RSC=rc.exe
RSC_PROJ=/l 0x41f /fo"$(INTDIR)\prjButton.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\prjButton.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\prjBtnGo.pdb" /machine:I386 /def:".\prjButton.def" /out:"$(OUTDIR)\prjBtnGo.dll" /implib:"$(OUTDIR)\prjBtnGo.lib" 
DEF_FILE= \
	".\prjButton.def"
LINK32_OBJS= \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\Edit.obj" \
	"$(INTDIR)\Mytoolbar.obj" \
	"$(INTDIR)\prjButton.obj" \
	"$(INTDIR)\ReflectionWnd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\XBand.obj" \
	"$(INTDIR)\prjButton.res"

"$(OUTDIR)\prjBtnGo.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\ReleaseUMinSize
TargetPath=.\ReleaseUMinSize\prjBtnGo.dll
InputPath=.\ReleaseUMinSize\prjBtnGo.dll
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	if "%OS%"=="" goto NOTNT 
	if not "%OS%"=="Windows_NT" goto NOTNT 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	goto end 
	:NOTNT 
	echo Warning : Cannot register Unicode DLL on Windows 95 
	:end 
<< 
	

!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"

OUTDIR=.\ReleaseUMinDependency
INTDIR=.\ReleaseUMinDependency
# Begin Custom Macros
OutDir=.\ReleaseUMinDependency
# End Custom Macros

ALL : "$(OUTDIR)\prjBtnGo.dll" ".\prjButton.tlb" ".\prjButton.h" ".\prjButton_i.c" ".\ReleaseUMinDependency\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Edit.obj"
	-@erase "$(INTDIR)\Mytoolbar.obj"
	-@erase "$(INTDIR)\prjButton.obj"
	-@erase "$(INTDIR)\prjButton.pch"
	-@erase "$(INTDIR)\prjButton.res"
	-@erase "$(INTDIR)\ReflectionWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\XBand.obj"
	-@erase "$(OUTDIR)\prjBtnGo.dll"
	-@erase "$(OUTDIR)\prjBtnGo.exp"
	-@erase "$(OUTDIR)\prjBtnGo.lib"
	-@erase ".\prjButton.h"
	-@erase ".\prjButton.tlb"
	-@erase ".\prjButton_i.c"
	-@erase ".\ReleaseUMinDependency\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_UNICODE" /D "_ATL_STATIC_REGISTRY" /D "_ATL_MIN_CRT" /Fp"$(INTDIR)\prjButton.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=
RSC=rc.exe
RSC_PROJ=/l 0x41f /fo"$(INTDIR)\prjButton.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\prjButton.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\prjBtnGo.pdb" /machine:I386 /def:".\prjButton.def" /out:"$(OUTDIR)\prjBtnGo.dll" /implib:"$(OUTDIR)\prjBtnGo.lib" 
DEF_FILE= \
	".\prjButton.def"
LINK32_OBJS= \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\Edit.obj" \
	"$(INTDIR)\Mytoolbar.obj" \
	"$(INTDIR)\prjButton.obj" \
	"$(INTDIR)\ReflectionWnd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\XBand.obj" \
	"$(INTDIR)\prjButton.res"

"$(OUTDIR)\prjBtnGo.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\ReleaseUMinDependency
TargetPath=.\ReleaseUMinDependency\prjBtnGo.dll
InputPath=.\ReleaseUMinDependency\prjBtnGo.dll
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	if "%OS%"=="" goto NOTNT 
	if not "%OS%"=="Windows_NT" goto NOTNT 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	goto end 
	:NOTNT 
	echo Warning : Cannot register Unicode DLL on Windows 95 
	:end 
<< 
	

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("prjButton.dep")
!INCLUDE "prjButton.dep"
!ELSE 
!MESSAGE Warning: cannot find "prjButton.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "prjButton - Win32 Debug" || "$(CFG)" == "prjButton - Win32 Unicode Debug" || "$(CFG)" == "prjButton - Win32 Release MinSize" || "$(CFG)" == "prjButton - Win32 Release MinDependency" || "$(CFG)" == "prjButton - Win32 Unicode Release MinSize" || "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"
SOURCE=.\Button.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"


"$(INTDIR)\Button.obj"	"$(INTDIR)\Button.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"


"$(INTDIR)\Button.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"


"$(INTDIR)\Button.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"


"$(INTDIR)\Button.obj"	"$(INTDIR)\Button.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch" ".\prjButton.h"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"


"$(INTDIR)\Button.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"


"$(INTDIR)\Button.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ENDIF 

SOURCE=.\Edit.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"


"$(INTDIR)\Edit.obj"	"$(INTDIR)\Edit.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"


"$(INTDIR)\Edit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"


"$(INTDIR)\Edit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"


"$(INTDIR)\Edit.obj"	"$(INTDIR)\Edit.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch" ".\prjButton.h"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"


"$(INTDIR)\Edit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"


"$(INTDIR)\Edit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ENDIF 

SOURCE=.\Mytoolbar.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"


"$(INTDIR)\Mytoolbar.obj"	"$(INTDIR)\Mytoolbar.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"


"$(INTDIR)\Mytoolbar.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"


"$(INTDIR)\Mytoolbar.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"


"$(INTDIR)\Mytoolbar.obj"	"$(INTDIR)\Mytoolbar.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"


"$(INTDIR)\Mytoolbar.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"


"$(INTDIR)\Mytoolbar.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ENDIF 

SOURCE=.\prjButton.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"


"$(INTDIR)\prjButton.obj"	"$(INTDIR)\prjButton.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"


"$(INTDIR)\prjButton.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"


"$(INTDIR)\prjButton.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"


"$(INTDIR)\prjButton.obj"	"$(INTDIR)\prjButton.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch" ".\prjButton_i.c" ".\prjButton.h"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"


"$(INTDIR)\prjButton.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"


"$(INTDIR)\prjButton.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ENDIF 

SOURCE=.\prjButton.idl

!IF  "$(CFG)" == "prjButton - Win32 Debug"

MTL_SWITCHES=/tlb ".\prjButton.tlb" /h "prjButton.h" /iid "prjButton_i.c" /Oicf 

".\prjButton.tlb"	".\prjButton.h"	".\prjButton_i.c" : $(SOURCE) "$(INTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"

MTL_SWITCHES=/tlb ".\prjButton.tlb" /h "prjButton.h" /iid "prjButton_i.c" /Oicf 

".\prjButton.tlb"	".\prjButton.h"	".\prjButton_i.c" : $(SOURCE) "$(INTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"

MTL_SWITCHES=/tlb ".\prjButton.tlb" /h "prjButton.h" /iid "prjButton_i.c" /Oicf 

".\prjButton.tlb"	".\prjButton.h"	".\prjButton_i.c" : $(SOURCE) "$(INTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"

MTL_SWITCHES=/tlb ".\prjButton.tlb" /h "prjButton.h" /iid "prjButton_i.c" /Oicf 

".\prjButton.tlb"	".\prjButton.h"	".\prjButton_i.c" : $(SOURCE) "$(INTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"

MTL_SWITCHES=/tlb ".\prjButton.tlb" /h "prjButton.h" /iid "prjButton_i.c" /Oicf 

".\prjButton.tlb"	".\prjButton.h"	".\prjButton_i.c" : $(SOURCE) "$(INTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"

MTL_SWITCHES=/tlb ".\prjButton.tlb" /h "prjButton.h" /iid "prjButton_i.c" /Oicf 

".\prjButton.tlb"	".\prjButton.h"	".\prjButton_i.c" : $(SOURCE) "$(INTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\prjButton.rc

"$(INTDIR)\prjButton.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\ReflectionWnd.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"


"$(INTDIR)\ReflectionWnd.obj"	"$(INTDIR)\ReflectionWnd.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"


"$(INTDIR)\ReflectionWnd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"


"$(INTDIR)\ReflectionWnd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"


"$(INTDIR)\ReflectionWnd.obj"	"$(INTDIR)\ReflectionWnd.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"


"$(INTDIR)\ReflectionWnd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"


"$(INTDIR)\ReflectionWnd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ENDIF 

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"

CPP_SWITCHES=/nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\prjButton.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\prjButton.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"

CPP_SWITCHES=/nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_UNICODE" /Fp"$(INTDIR)\prjButton.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\prjButton.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"

CPP_SWITCHES=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_DLL" /Fp"$(INTDIR)\prjButton.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\prjButton.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"

CPP_SWITCHES=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_STATIC_REGISTRY" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\prjButton.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\prjButton.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"

CPP_SWITCHES=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_UNICODE" /D "_ATL_DLL" /D "_ATL_MIN_CRT" /Fp"$(INTDIR)\prjButton.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\prjButton.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"

CPP_SWITCHES=/nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_UNICODE" /D "_ATL_STATIC_REGISTRY" /D "_ATL_MIN_CRT" /Fp"$(INTDIR)\prjButton.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\prjButton.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\XBand.cpp

!IF  "$(CFG)" == "prjButton - Win32 Debug"


"$(INTDIR)\XBand.obj"	"$(INTDIR)\XBand.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Debug"


"$(INTDIR)\XBand.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinSize"


"$(INTDIR)\XBand.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Release MinDependency"


"$(INTDIR)\XBand.obj"	"$(INTDIR)\XBand.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch" ".\prjButton.h"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinSize"


"$(INTDIR)\XBand.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ELSEIF  "$(CFG)" == "prjButton - Win32 Unicode Release MinDependency"


"$(INTDIR)\XBand.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\prjButton.pch"


!ENDIF 


!ENDIF 

