/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Thu May 23 16:24:47 2019
 */
/* Compiler settings for C:\Documents and Settings\x41\Belgelerim\Visual Studio Projects\URL_incrementer\prjButton.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IXBand = {0x9464C231,0xDF18,0x409B,{0x86,0x6E,0x89,0x48,0x96,0xCC,0x72,0xDA}};


const IID LIBID_PRJBUTTONLib = {0xD475AE4C,0xBDFC,0x49B8,{0xB0,0xE9,0x89,0x43,0xB7,0xD6,0x1D,0xA9}};


const CLSID CLSID_XBand = {0x07B0B471,0xA591,0x43D4,{0xA9,0xE1,0xEC,0xAB,0xA7,0xA3,0x77,0x82}};


#ifdef __cplusplus
}
#endif

