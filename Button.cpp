//Button.cpp : Implementation of CButton

//***************************************************************************//
//                                                                           //
//  This file was created using the CWindowImpl ATL Object Wizard            //
//  By Erik Thompson � 2000                                                  //
//	Version 1.1                                                              //
//  Email questions and comments to ErikT@RadBytes.com                       //
//                                                                           //
//***************************************************************************//

#include "stdafx.h"
#include "Button.h"
#include "prjButton.h"
#include "XBand.h"

/////////////////////////////////////////////////////////////////////////////
// CButton

CButton::CButton() : m_pBand(NULL)
{
}

CButton::~CButton()
{
}

void CButton::SetBand(CXBand* pBand)
{
	m_pBand = pBand;
}
