//Button.h : Declaration of the CButton

//***************************************************************************//
//                                                                           //
//  This file was created using the CWindowImpl ATL Object Wizard            //
//  By Erik Thompson � 2000                                                  //
//	Version 1.1                                                              //
//  Email questions and comments to ErikT@RadBytes.com                       //
//                                                                           //
//***************************************************************************//

#ifndef __BUTTON_H_
#define __BUTTON_H_

#include <commctrl.h>

class CXBand;
/////////////////////////////////////////////////////////////////////////////
// CButton
class CButton : public CWindowImpl<CButton>
{
public:

	
	DECLARE_WND_SUPERCLASS(TEXT("btnX"), TEXT("BUTTON"))

	BEGIN_MSG_MAP(CButton)
	END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	CButton();
	virtual ~CButton();
	void SetBand(CXBand* pBand);
private:
	CXBand* m_pBand;
};

#endif //__BUTTON_H_