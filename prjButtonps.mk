
prjButtonps.dll: dlldata.obj prjButton_p.obj prjButton_i.obj
	link /dll /out:prjButtonps.dll /def:prjButtonps.def /entry:DllMain dlldata.obj prjButton_p.obj prjButton_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del prjButtonps.dll
	@del prjButtonps.lib
	@del prjButtonps.exp
	@del dlldata.obj
	@del prjButton_p.obj
	@del prjButton_i.obj
