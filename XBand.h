//XBand.h : Declaration of the CXBand

//***************************************************************************//
//                                                                           //
//  This file was created using the DeskBand ATL Object Wizard 2.0           //
//  By Erik Thompson � 2001                                                  //
//  Email questions and comments to erikt@radbytes.com						 //
//                                                                           //
//***************************************************************************//

#ifndef __XBAND_H_
#define __XBAND_H_

#include "resource.h"       // main symbols
#include "ReflectionWnd.h"
//
// These are needed for IDeskBand
//

#include <shlguid.h>
#include <shlobj.h>

/////////////////////////////////////////////////////////////////////////////
// CXBand
class ATL_NO_VTABLE CXBand : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CXBand, &CLSID_XBand>,
	public IDeskBand,
	public IObjectWithSite,
	public IInputObject,
	public IDispatchImpl<IXBand, &IID_IXBand, &LIBID_PRJBUTTONLib>
{
public:
	CXBand();

DECLARE_REGISTRY_RESOURCEID(IDR_XBAND)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_CATEGORY_MAP(CXBand)
//	IMPLEMENTED_CATEGORY(CATID_InfoBand)
//	IMPLEMENTED_CATEGORY(CATID_CommBand)
//	IMPLEMENTED_CATEGORY(CATID_DeskBand)
END_CATEGORY_MAP()

BEGIN_COM_MAP(CXBand)
	COM_INTERFACE_ENTRY(IXBand)
	COM_INTERFACE_ENTRY(IInputObject)
	COM_INTERFACE_ENTRY(IOleWindow)
	COM_INTERFACE_ENTRY_IID(IID_IDockingWindow, IDockingWindow)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY_IID(IID_IDeskBand, IDeskBand)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IDeskBand
public:
	STDMETHOD(GetBandInfo)(DWORD dwBandID, DWORD dwViewMode, DESKBANDINFO* pdbi);

// IObjectWithSite
public:
	STDMETHOD(SetSite)(IUnknown* pUnkSite);
	STDMETHOD(GetSite)(REFIID riid, void **ppvSite);

// IOleWindow
public:
	STDMETHOD(GetWindow)(HWND* phwnd);
	STDMETHOD(ContextSensitiveHelp)(BOOL fEnterMode);

// IDockingWindow
public:
	STDMETHOD(CloseDW)(unsigned long dwReserved);
	STDMETHOD(ResizeBorderDW)(const RECT* prcBorder, IUnknown* punkToolbarSite, BOOL fReserved);
	STDMETHOD(ShowDW)(BOOL fShow);

// IInputObject

public:
   STDMETHOD(HasFocusIO)(void);
   STDMETHOD(TranslateAcceleratorIO)(LPMSG lpMsg);
   STDMETHOD(UIActivateIO)(BOOL fActivate, LPMSG lpMsg);


// IXBand
public:
	void FocusChange(BOOL bHaveFocus);


protected:
	BOOL RegisterAndCreateWindow();
protected:
	DWORD m_dwBandID;
	DWORD m_dwViewMode;
	BOOL m_bShow;
	BOOL m_bEnterHelpMode;
	HWND m_hWndParent;
	//HWND m_hWnd;
	CReflectionWnd m_ReflectWnd;
	IInputObjectSite* m_pSite;
};

#endif //__XBAND_H_