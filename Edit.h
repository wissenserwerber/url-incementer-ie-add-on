//Edit.h : Declaration of the CEdit

//***************************************************************************//
//                                                                           //
//  This file was created using the CWindowImpl ATL Object Wizard            //
//  By Erik Thompson � 2000                                                  //
//	Version 1.1                                                              //
//  Email questions and comments to ErikT@RadBytes.com                       //
//                                                                           //
//***************************************************************************//

#ifndef __EDIT_H_
#define __EDIT_H_

#include <commctrl.h>

class CXBand;
/////////////////////////////////////////////////////////////////////////////
// CEdit
class CEdit : public CWindowImpl<CEdit>
{
public:
	DECLARE_WND_SUPERCLASS(TEXT("EDITX"), TEXT("EDIT"))

	BEGIN_MSG_MAP(CEdit)
		COMMAND_CODE_HANDLER(EN_SETFOCUS, OnSetFocus)
	END_MSG_MAP()

//  Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);
	LRESULT OnSetFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	
	CEdit();
	virtual ~CEdit();
	STDMETHOD(TranslateAcceleratorIO)(LPMSG lpMsg);
	void SetBand(CXBand* pBand);

private:
	CXBand* m_pBand;
};

#endif //__EDIT_H_