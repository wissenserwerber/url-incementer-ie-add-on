//Mytoolbar.h : Declaration of the CMytoolbar

//***************************************************************************//
//                                                                           //
//  This file was created using the CWindowImpl ATL Object Wizard            //
//  By Erik Thompson � 2000                                                  //
//	Version 1.1                                                              //
//  Email questions and comments to ErikT@RadBytes.com                       //
//                                                                           //
//***************************************************************************//

#ifndef __MYTOOLBAR_H_
#define __MYTOOLBAR_H_

#include <commctrl.h>
#include "Button.h"
#include "Edit.h"
#define timeinterval 1000
/////////////////////////////////////////////////////////////////////////////
// CMytoolbar
class CMytoolbar : public CWindowImpl<CMytoolbar>
{
public:
	
	DECLARE_WND_SUPERCLASS(TEXT("MYTOOLBAR"), TOOLBARCLASSNAME)

	BEGIN_MSG_MAP(CMytoolbar)
	CHAIN_MSG_MAP_MEMBER(m_Button)
	CHAIN_MSG_MAP_MEMBER(m_Edit)
	CHAIN_MSG_MAP_MEMBER(m_Reset)
	CHAIN_MSG_MAP_MEMBER(m_Inkrementieren)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_COMMAND, OnCommand)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	MESSAGE_HANDLER(WM_DESTROY, KillTheTimer)
	END_MSG_MAP()

// Handler prototypes:

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT KillTheTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	CMytoolbar();
	virtual ~CMytoolbar();
	inline CEdit& GetEdit() {return m_Edit;};
	void SetBrowser(IWebBrowser2* pBrowser);

	UINT_PTR m_timer;

private:
	CButton m_Button;
	CButton m_Reset;
	CButton m_Inkrementieren;
	CEdit m_Edit;
	IWebBrowser2* m_pBrowser;
	void GoForward();
	void Reset();
	void Inkrementieren();
	int gStart, gEnd;
	bool flg;
	HWND hwndAddressBar;
	// int endeks;
	char* URL;
	char* URL_schnitt;
	TCHAR start[3];
	TCHAR end[3];
	int URL_length;
	char* URL_temp;
};
#endif //__MYTOOLBAR_H_