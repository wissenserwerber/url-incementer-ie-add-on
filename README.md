## URL Incrementer (IE Add-on)

Eine Add-on zum automatisch Inkrementieren der URL. Manchmal sto�en Sie auf so f�r eine Webseite, die nur Zeichnissen und Dateiennamen anbietet. Das kommt sehr h�ufig bei der default Directory Index Eigenschaft des Apache Web Servers vor. In solchen F�llen m�ssen Sie st�ndig die Dateiennamen aus dem Verzeichnis klicken, um sie die durchzuschauen. Dieses experimentale Projekt bietet eine IE Add-on um diese Aufgabe zu automatisieren.

![Bildschirmaufnahme](URL_incrementer.png)

Es wurde in C++ mittels Visual C++ entwickelt.

Schauen Sie das Video an um zu sehen wie es werwendet wird.

![Demo Video](Bildschirm_Aufnahme.mp4)

Projekt:
git@gitlab.com:wissenserwerber/url-incementer-ie-add-on.git
