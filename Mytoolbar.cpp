//Mytoolbar.cpp : Implementation of CMytoolbar
//***************************************************************************//
//  This file was created using the CWindowImpl ATL Object Wizard            //
//  By Erik Thompson � 2000                                                  //
//	Version 1.1                                                          //
//  Email questions and comments to ErikT@RadBytes.com                       //
//***************************************************************************//

#include "stdafx.h"
#include "resource.h"
#include "Mytoolbar.h"
#include "string.h"
/////////////////////////////////////////////////////////////////////////////

CMytoolbar::CMytoolbar() : m_pBrowser(NULL)
{}

CMytoolbar::~CMytoolbar() 
{
  SetBrowser(NULL);
  if (IsWindow()) DestroyWindow();
}

LRESULT CMytoolbar::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) 
{
  m_timer = SetTimer(1, timeinterval); 
  // m_hWnd definiert in der ATLWIN.h 
  SendMessage(m_hWnd, TB_SETEXTENDEDSTYLE, 0,(LPARAM)TBSTYLE_BUTTON);
  SendMessage(m_hWnd, TB_BUTTONSTRUCTSIZE, sizeof(TBBUTTON), 0);
  SendMessage(m_hWnd, TB_SETMAXTEXTROWS, 1, 0L);
	
  TCHAR* pCaption = _T("Reset");
  int iIndex = ::SendMessage(m_hWnd, TB_ADDSTRING, 0,(LPARAM)pCaption);

  /* Die Schaltfl�che um AdressLeiste in den m_Edit einzuschieben*/
  RECT rectButton = {1,1,21,21};
  m_Button.Create(m_hWnd, rectButton, NULL, WS_CHILD|WS_VISIBLE, WS_EX_CLIENTEDGE);
  m_Button.SetDlgCtrlID(IDM_Forward);
	
  /* Der Textfeld zum Aushalten der URL*/
  RECT rectEdit = {30,1,300,21};
  m_Edit.Create(m_hWnd,rectEdit,NULL,WS_CHILD|WS_VISIBLE,WS_EX_CLIENTEDGE);
  m_Edit.SetFont(static_cast<HFONT>(GetStockObject(DEFAULT_GUI_FONT)));
  m_Edit.SetDlgCtrlID(IDM_Edit); // Das ist notwending um das Klicken Reset zum Ereignis Reset zu verkn�pfen
  // m_Edit.SetDlgCtrlID(303);
	
  /* Die Schaltfl�che zum Eintragen des ausgew�hlten Texts in den Textfeld */
  RECT rectReset = {303,1,323,21};
  m_Reset.Create(m_hWnd, rectReset, NULL, WS_CHILD|WS_VISIBLE, WS_EX_CLIENTEDGE);
  static HWND hbtnReset = GetDlgItem(IDM_Reset);	
  m_Reset.SetDlgCtrlID(IDM_Reset);

  /*  Die Schaltfl�che zum Inkrementieren der URL*/
  RECT rectInkrementieren = {326,1,346,21};
  m_Inkrementieren.Create(m_hWnd, rectInkrementieren, NULL, WS_CHILD|WS_VISIBLE, WS_EX_CLIENTEDGE);
  static HWND hbtnInkrementieren = GetDlgItem(IDM_Inkrementieren);	
  m_Inkrementieren.SetDlgCtrlID(IDM_Inkrementieren);

  ::SendMessage(m_hWnd, TB_INSERTBUTTON, 0, (LPARAM)&hbtnReset);

  return 0;
}

LRESULT CMytoolbar::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  return 0;
}

void CMytoolbar::SetBrowser(IWebBrowser2* pBrowser)
{
  if (m_pBrowser) m_pBrowser->Release();
  m_pBrowser = pBrowser;
  if (m_pBrowser) m_pBrowser->AddRef();
}


LRESULT CMytoolbar::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  //Decide what to when pressed different buttons.
  if (!HIWORD(wParam))
  {
	  long lSite = LOWORD(wParam);
      if (lSite == IDM_Forward)
	  {
		  if (m_pBrowser)
		  {
			  GoForward();
		  }
		  return 0;
	  }
	  if (lSite == IDM_Reset)
	  {
		  if (m_pBrowser)
		  {
			  Reset();
		  }
		  return 0;
	  }
	  if (lSite == IDM_Inkrementieren)
	  {
		  if (m_pBrowser)
		  {
			  Inkrementieren();
		  }
		  return 0;
	  }
  }
  
  return -1;
}

void CMytoolbar::GoForward()
{

  static HWND hEdit = m_Edit.GetWindow(GW_HWNDLAST);
  static HWND hBtnReset = GetDlgItem(IDM_Reset);
  static HWND hMyEdit = GetDlgItem(IDM_Edit);
  m_pBrowser->Stop();


  //char strCurrentURL[255];
  BSTR bstrURL;
  HRESULT temp = m_pBrowser->get_LocationURL(&bstrURL);
  
  // bstrURL ==> Die derzeitige URL 
  
  char *strCurrentURL = _com_util::ConvertBSTRToString(bstrURL);
  
  ::SendMessage(hMyEdit,WM_SETTEXT,0,(long)strCurrentURL);

  // char tmp[5];
  URL_length = ::GetWindowTextLength(hMyEdit);
  // itoa(URL_length,tmp,10);
  URL = (char*) malloc(URL_length * sizeof(char));
  SysFreeString(bstrURL);
  
}

LRESULT CMytoolbar::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
  ::SendMessage(hwndAddressBar,CB_GETEDITSEL,(WPARAM) &gStart,(LPARAM) &gEnd);
  return 0;
}

LRESULT CMytoolbar::KillTheTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{KillTimer(m_timer);return 0;}

void CMytoolbar::Reset(){
	
  VARIANT vEmpty;
  VariantInit(&vEmpty);

  USES_CONVERSION;


	static HWND hMyEdit = GetDlgItem(IDM_Edit);
	
	::GetWindowText(hMyEdit, URL, 1024);
	
	::SendMessage(hMyEdit,EM_GETSEL,(WPARAM) &gStart,(LPARAM) &gEnd);

	itoa(gStart, start, 10);
	itoa(gEnd, end, 10);
	int groesse = gEnd - gStart + 1;
	/*The sizes of array variables in C must be known at compile time. 
	If you know it only at run time you will have to malloc some memory yourself */
	// TCHAR sel[groesse];
	
	URL_schnitt = (char*) malloc(groesse * sizeof(char));
	
	int y=0;
	for (int i = gStart; i<gEnd; i++) {
		URL_schnitt[y] = URL[i];
		y++;
	}
  
	URL_schnitt[groesse-1] = '\0'; // Den Null terminator hinsetzen; das ist notwendig
	
	
	// strcat(start, " - "); //Verursacht Oveflow-Fehler

	::SendMessage(hMyEdit,WM_SETTEXT,0,(long)URL_schnitt);//);
	
	return;
}

void CMytoolbar::Inkrementieren(){
	
  VARIANT vEmpty;
  VariantInit(&vEmpty);

  USES_CONVERSION;
  int endeks = atoi(URL_schnitt);
  endeks++;
  itoa(endeks, URL_schnitt, 10);

  URL_temp = (char*) malloc(1+URL_length * sizeof(char));
  int y=0;
  for (int i=0; i<URL_length; i++) {
	  if (i>= gStart && i<gEnd) {
		  URL_temp[i]=URL_schnitt[y];
		  y++;}
	  else {
		  URL_temp[i] = URL[i];
	  }
  }

  URL_temp[URL_length] = '\0'; // Den Null terminator hinsetzen; das ist notwendig
	
  CComBSTR ccomBSTR(URL_temp);
  BSTR BSTRURL = ccomBSTR;
  _bstr_t bstrURLFinal(BSTRURL);

  m_pBrowser->Navigate(bstrURLFinal, &vEmpty, &vEmpty, &vEmpty, &vEmpty);
}