//Edit.cpp : Implementation of CEdit

//***************************************************************************//
//                                                                           //
//  This file was created using the CWindowImpl ATL Object Wizard            //
//  By Erik Thompson � 2000                                                  //
//	Version 1.1                                                              //
//  Email questions and comments to ErikT@RadBytes.com                       //
//                                                                           //
//***************************************************************************//

#include "stdafx.h"
#include "Edit.h"
#include "prjButton.h"
#include "XBand.h"

/////////////////////////////////////////////////////////////////////////////
// CEdit

CEdit::CEdit()
{
}

CEdit::~CEdit()
{
}

STDMETHODIMP CEdit::TranslateAcceleratorIO(LPMSG lpMsg)
{
	int nVirtKey = (int)(lpMsg->wParam);
	if (VK_RETURN == nVirtKey)
	{
		lpMsg->wParam = 0;
		::PostMessage(GetParent(), IDM_Forward, 0, 0);
		return S_OK;
	}
	else if (WM_KEYDOWN == lpMsg->message && nVirtKey == VK_TAB)
	{
		if (m_pBand) m_pBand->FocusChange(FALSE);
		return S_FALSE;
	}
	TranslateMessage(lpMsg);
	DispatchMessage(lpMsg);
	return S_OK;
}

LRESULT CEdit::OnSetFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	if (m_pBand) m_pBand->FocusChange(TRUE);
	return 0;
}

void CEdit::SetBand(CXBand* pBand)
{
	m_pBand = pBand;
}


