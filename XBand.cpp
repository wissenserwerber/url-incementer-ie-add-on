//XBand.cpp : Implementation of CXBand

//***************************************************************************//
//                                                                           //
//  This file was created using the DeskBand ATL Object Wizard 2.0           //
//  By Erik Thompson � 2001                                                  //
//  Email questions and comments to erikt@radbytes.com						 //
//                                                                           //
//***************************************************************************//

#include "stdafx.h"
#include <wchar.h>
#include "PrjButton.h"
#include "XBand.h"

const WCHAR TITLE_CXBand[] = L"";

/////////////////////////////////////////////////////////////////////////////
// CXBand

CXBand::CXBand(): 
	m_dwBandID(0), 
	m_dwViewMode(0), 
	m_bShow(FALSE), 
	m_bEnterHelpMode(FALSE), 
	m_hWndParent(NULL),
	m_pSite(NULL)
{
		m_ReflectWnd.GetToolbar().GetEdit().SetBand(this); //Burasi cok �nemli (fokus)
}

BOOL CXBand::RegisterAndCreateWindow()
{
	RECT rect;
	::GetClientRect(m_hWndParent, &rect);

	//
	// TODO: Replace with your implementation
	//
m_ReflectWnd.Create(m_hWndParent, rect, NULL, WS_CHILD);
	/*
	m_hWnd = ::CreateWindow(TEXT("STATIC"), 
				TEXT("The CXBand DeskBand"), 
				WS_CHILD | WS_VISIBLE | SS_CENTER,
				rect.left, rect.top, 
				rect.right - rect.left, 
				rect.bottom - rect.top, 
				m_hWndParent, 
				NULL, 
				NULL, 
				NULL);
	return ::IsWindow(m_hWnd);
	*/
return m_ReflectWnd.GetToolbar().IsWindow();
}

// IDeskBand
STDMETHODIMP CXBand::GetBandInfo(DWORD dwBandID, DWORD dwViewMode, DESKBANDINFO* pdbi)
{
	m_dwBandID = dwBandID;
	m_dwViewMode = dwViewMode;

	if (pdbi)
	{
		if (pdbi->dwMask & DBIM_MINSIZE)
		{
			pdbi->ptMinSize.x = 250;
			pdbi->ptMinSize.y = 20;
		}
		if (pdbi->dwMask & DBIM_MAXSIZE)
		{
			pdbi->ptMaxSize.x = 0; // ignored
			pdbi->ptMaxSize.y = -1;	// width
		}
		if (pdbi->dwMask & DBIM_INTEGRAL)
		{
			pdbi->ptIntegral.x = 1; // ignored
			pdbi->ptIntegral.y = 1; // not sizeable
		}
		if (pdbi->dwMask & DBIM_ACTUAL)
		{
			pdbi->ptActual.x = 250;
			pdbi->ptActual.y = 20;
		}
		if (pdbi->dwMask & DBIM_TITLE)
		{
			wcscpy(pdbi->wszTitle, TITLE_CXBand);
		}
		if (pdbi->dwMask & DBIM_BKCOLOR)
		{
			//Use the default background color by removing this flag.
			pdbi->dwMask &= ~DBIM_BKCOLOR;
		}
		if (pdbi->dwMask & DBIM_MODEFLAGS)
		{
			pdbi->dwModeFlags = DBIMF_VARIABLEHEIGHT;
		}
	}
	return S_OK;
}

// IOleWindow
STDMETHODIMP CXBand::GetWindow(HWND* phwnd)
{
	HRESULT hr = S_OK;
	if (NULL == phwnd)
	{
		hr = E_INVALIDARG;
	}
	else
	{
		*phwnd = m_ReflectWnd.GetToolbar().m_hWnd;
	}
	return hr;
}

STDMETHODIMP CXBand::ContextSensitiveHelp(BOOL fEnterMode)
{
	m_bEnterHelpMode = fEnterMode;
	return S_OK;
}

// IDockingWindow
STDMETHODIMP CXBand::CloseDW(unsigned long dwReserved)
{
	/*
	if (::IsWindow(m_hWnd))
	{
		::DestroyWindow(m_hWnd);
	}
	*/
	ShowDW(FALSE);
	return S_OK;
}

STDMETHODIMP CXBand::ResizeBorderDW(const RECT* prcBorder, IUnknown* punkToolbarSite, BOOL fReserved)
{
	// Not used by any band object.
	return E_NOTIMPL;
}

STDMETHODIMP CXBand::ShowDW(BOOL fShow)
{
	HRESULT hr = S_OK;
	m_bShow = fShow;
	m_ReflectWnd.GetToolbar().ShowWindow(m_bShow ? SW_SHOW : SW_HIDE);
	return hr;
}
// IObjectWithSite
STDMETHODIMP CXBand::SetSite(IUnknown* pUnkSite)
{
//If a site is being held, release it.
	if(m_pSite)
	{
		m_ReflectWnd.GetToolbar().SetBrowser(NULL);  //OK
		m_pSite->Release();
		m_pSite = NULL;
	}

	//If punkSite is not NULL, a new site is being set.
	/*
	if(pUnkSite)
	{
		//Get the parent window.
		IOleWindow  *pOleWindow = NULL;

		m_hWndParent = NULL;

		if(SUCCEEDED(pUnkSite->QueryInterface(IID_IOleWindow, (LPVOID*)&pOleWindow)))
		{
			pOleWindow->GetWindow(&m_hWndParent);
			pOleWindow->Release();
		}

		if(!::IsWindow(m_hWndParent))
			return E_FAIL;

		if(!RegisterAndCreateWindow())
			return E_FAIL;

		//Get and keep the IInputObjectSite pointer.
		if(SUCCEEDED(pUnkSite->QueryInterface(IID_IInputObjectSite, (LPVOID*)&m_pSite)))
		{
			return S_OK;
		}  
		return E_FAIL;
	}
	*/



		//If punkSite is not NULL, a new site is being set.
	if(pUnkSite)
	{
		//Get the parent window.
		IOleWindow  *pOleWindow = NULL;

		m_hWndParent = NULL;

		if(SUCCEEDED(pUnkSite->QueryInterface(IID_IOleWindow, (LPVOID*)&pOleWindow)))
		{
			pOleWindow->GetWindow(&m_hWndParent);
			pOleWindow->Release();
		}

		if(!::IsWindow(m_hWndParent))
			return E_FAIL;

		if(!RegisterAndCreateWindow())
			return E_FAIL;

		//Get and keep the IInputObjectSite pointer.
		if(FAILED(pUnkSite->QueryInterface(IID_IInputObjectSite, (LPVOID*)&m_pSite)))
		{
			return E_FAIL;
		}  

		IWebBrowser2* s_pFrameWB = NULL;
		IOleCommandTarget* pCmdTarget = NULL;
		HRESULT hr = pUnkSite->QueryInterface(IID_IOleCommandTarget, (LPVOID*)&pCmdTarget);
		if (SUCCEEDED(hr))
		{
			IServiceProvider* pSP;
			hr = pCmdTarget->QueryInterface(IID_IServiceProvider, (LPVOID*)&pSP);

			pCmdTarget->Release();

			if (SUCCEEDED(hr))
			{
				hr = pSP->QueryService(SID_SWebBrowserApp, IID_IWebBrowser2, (LPVOID*)&s_pFrameWB);
				pSP->Release();
				_ASSERT(s_pFrameWB);
				m_ReflectWnd.GetToolbar().SetBrowser(s_pFrameWB);
				s_pFrameWB->Release();
			}
		}
	}


	return S_OK;
}

STDMETHODIMP CXBand::GetSite(REFIID riid, void **ppvSite)
{
	*ppvSite = NULL;

	if(m_pSite)
	{
	   return m_pSite->QueryInterface(riid, ppvSite);
	}
	return E_FAIL;
}

void CXBand::FocusChange(BOOL bHaveFocus)
{
	if (m_pSite)
	{
		IUnknown* pUnk = NULL;
		if (SUCCEEDED(QueryInterface(IID_IUnknown, (LPVOID*)&pUnk)) && pUnk != NULL)
		{
			m_pSite->OnFocusChangeIS(pUnk, bHaveFocus);
			pUnk->Release();
			pUnk = NULL;
		}
	}
}

STDMETHODIMP CXBand::HasFocusIO(void)
{
   // if any of the windows in our toolbar have focus then return S_OK else S_FALSE.
	if (m_ReflectWnd.GetToolbar().m_hWnd == ::GetFocus())
		return S_OK;
	if (m_ReflectWnd.GetToolbar().GetEdit().m_hWnd == ::GetFocus())
		return S_OK;
	return S_FALSE;
}
STDMETHODIMP CXBand::TranslateAcceleratorIO(LPMSG lpMsg)
{
   // the only window that needs to translate messages is our edit box so forward them.

   return m_ReflectWnd.GetToolbar().GetEdit().TranslateAcceleratorIO(lpMsg);
}
STDMETHODIMP CXBand::UIActivateIO(BOOL fActivate, LPMSG lpMsg)
{
   // if our deskband is being activated then set focus to the edit box.

   if(fActivate)
   {
      m_ReflectWnd.GetToolbar().GetEdit().SetFocus();
   }
   return S_OK;
}
